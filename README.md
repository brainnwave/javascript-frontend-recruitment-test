# Brainnwave JavaScript Recruitment Test

Thanks for taking the time to do our front-end test. The challenge has two parts:

1) a task to create a basic front-end application that displays data from a provided geoJSON data set.

2) some [follow-up questions](./FOLLOW-UP.md)

----

Feel free to spend as much or as little time as you'd like, as long as the following criteria have been met:

* Your implementation works as described in the task, meeting all the specified functionality.

* Your solution looks like the provided design.

----

## Important Note

Please do not give this test to your recruiter. It is important that the test remains private.

## Task

At Brainnwave we take lunch very seriously. For this reason it is vital that we stay on top of every eatery and food option around our New Town office. Your task is to assist us by building a Web app that lists available food options around our office.

- Use the provided geoJSON data set (the data is a small extract from OpenStreetMap) to create a map of food and drink establishments in Edinburgh

- The establishments need to be filterable by 'type of eatery' and 'type of food'. Using these filters will change the results displayed on both the map and the list views. The key properties that will help guide the filtering are `amenity` and `shop` (which are mutually exclusive), as well as `name` (which is present on almost every object) and `cuisine` (which is often missing).

- A text search. This allows searching of specific place names and will update the map/list view accordingly

- Ensure your application matches the provided design.

## Design

We've provided a few [design](./designs/) images for the application. Don't worry about mobile device screen sizes, but **please make sure your solution looks good at landscape orientation on a small laptop size screen and scales for larger laptop screen sizes**.

We have specified which fonts to use - any fonts are either native browser fonts or they can be found free to use on [Google Fonts](https://fonts.google.com/)

We have added the measurements and hex codes to the designs

## Bonus Functionality

We have some ideas for features that would enhance the application. These are not necessary for completion of the task but will give you bonus points. If you have the time to add these, feel free to do so.

- get the user's current location using the Geolocation API and display it on the map

- sort the list view by distance from the user's current location

- add pagination to the list view

- clicking an eatery on the list view will show the eatery on the map view

- implement a state management tool like redux to handle shared state

## Implementation

We'd like you to use [React](https://facebook.github.io/react/). On top of that, use whatever front-end libraries you feel comfortable with.

We've set you up with a build based on Facebook's [create-react-app](https://github.com/facebookincubator/create-react-app). To run the client:

- `yarn` to install any packages (run this once)

- `yarn start` to start the client development build at [http://localhost:3000](http://localhost:3000)

Feel free to use whatever mapping software you are most comfortable with. Some options are [Leaflet.js](https://leafletjs.com/), [Mapbox](https://www.mapbox.com/) and [Google Maps](https://cloud.google.com/maps-platform/).

We've wired in [Sass](http://sass-lang.com/).

## Data Set

We have provided you with a geoJSON data set which is located at `/dataset/data.geojson`

This data consists of a number of different types of food and drink establishments in Edinburgh. They each have latitude/longitude co-ordinates and a varying level of associated data like an 'amenity' type or a 'phone' number.

## Submission Guidelines

* Please submit your program by either: 

1) Uploading it to a private BitBucket or GitHub repository.
   
2) Uploading it to Dropbox

* The repository should be named {yourname}-brainnwave-challenge, and contain all the files for your submission.

* The repository or dropbox folder should contain the [FOLLOW-UP.md](./FOLLOW-UP.md) file with answers to the follow-up questions.

* The repository or dropbox folder should **not** include the `node_modules` folder.

* Please let your Brainnwave contact know when you've uploaded your solution.

* You will need to ensure your Brainnwave contact has access to the private repository or dropbox folder.

----

Inspiration for the test format taken from [Skyscanner's recruitment test](https://github.com/Skyscanner/full-stack-recruitment-test/).