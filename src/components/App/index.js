import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App__header">
          <h1 className="App__title">Welcome to BrainnWave's JavaScript Recruitment Test</h1>
        </header>
        <p className="App__intro">
          To get started, read the instructions in the <code>README.md</code> file
        </p>
      </div>
    );
  }
}

export default App;
